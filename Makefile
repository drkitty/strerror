MAKEFLAGS += --no-builtin-rules --warn-undefined-variables
.SUFFIXES:
.DELETE_ON_ERROR:

.DEFAULT_GOAL = $(EXE)
.PHONY: clean


EXE := strerror
SRC := $(EXE).c fail.c

CC := gcc $(if $(DEBUG),-DDEBUG) \
	-std=c99 -pedantic -g -Wall -Wextra -Werror \
	$(if $(STRICT),-Wunused,-Wno-unused)


$(EXE): $(SRC)
	$(CC) -o $@ $^

clean:
	rm -f $(EXE)
