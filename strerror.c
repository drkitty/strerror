#include "common.h"

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fail.h"


int verbosity = 0;


void exit_with_usage(void)
{
    print("Usage:  strerror ERRNO\n");
    exit(E_USAGE);
}


int main(int argc, char** argv)
{
    if (argc != 2 || isspace(argv[1][0]))
        exit_with_usage();

    errno = 0;
    char* end;
    long errnum = strtol(argv[1], &end, 10);
    if (errno != 0 || *end != 0 || errnum < INT_MIN || errnum > INT_MAX)
        exit_with_usage();

    if (errnum < 0)
        errnum = -errnum;

    printf("\"%s\"\n", strerror(errnum));

    return 0;
}
